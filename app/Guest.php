<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    protected $fillable = [
        'name',
        'nip',
        'phone',
        'email',
        'address',
        'name_institusi',
        'user_id'
    ];

    protected $hidden = [
        'user_id'
    ];
    
    public function user() {
        return $this->belongsTo('App\User');
    }
}
