<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Season extends Model
{
    protected $fillable = [
        'name',
        'start_date',
        'end_date'
    ];
    
    public function room_types()
    {
        return $this->belongsToMany('App\RoomType')->withPivot(['type', 'amount']);
    }
}
