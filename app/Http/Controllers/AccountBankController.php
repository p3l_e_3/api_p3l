<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\AccountBank;
use Illuminate\Http\Request;

class AccountBankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return AccountBank::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'account_number' => 'required|numeric|digits_between:10,15|unique:account_banks,account_number',
            'account_name' => 'required',
            'type' => 'required|in:bca,bni,cimb_niaga,bri',
        ]);

        if ($validation->fails()) {
            return response()
            ->json($validation->errors(), 422);
        }

        $user_data = $request->all();

        try {
            DB::transaction(function () use ($user_data, &$account_bank) {
            $account_bank = AccountBank::create($user_data);
            });

            return AccountBank::find($account_bank->account_number);
        } catch(\Exception $e) {
            return response()
            ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AccountBank  $accountBank
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $account_bank = AccountBank::find($id);

        if(is_null($account_bank)) {
            return response()
            ->json(['errors' => ['account_bank_not_found']], 404);
        }

        $validation = Validator::make($request->all(), [
            'account_name' => 'sometimes',
            'type' => 'sometimes|in:bca,bni,cimb_niaga,bri',
        ]);

        if ($validation->fails()) {
            return response()
            ->json($validation->errors(), 422);
        }

        $user_data = $request->only('account_name', 'type');

        try {
            DB::transaction(function () use ($user_data, $account_bank) {
            $account_bank->update($user_data);
            });

            return AccountBank::find($account_bank->account_number);
        } catch(\Exception $e) {
            return response()
            ->json(['errors' => [$e->getMessage()]], 500);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AccountBank  $accountBank
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $account_bank = AccountBank::find($id);

        if(is_null($account_bank)) {
            return response()
            ->json(['errors' => ['account_bank_not_found']], 404);
        }

        $account_bank->delete();
        
        return response()->json();
    }

}
