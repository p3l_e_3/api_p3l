<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Reservation;
use App\Guest;
use App\AccountBank;
use App\CreditCard;
use App\Payment;
use App\Transaction;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
    public function index()
    {
        return Reservation::withTrashed()->with(['rooms', 'guest', 'payment', 'transaction'])->get();
    }

    public function get($id)
    {
        $reservation = Reservation::with(['rooms', 'guest', 'payment', 'transaction'])->find($id);

        if(is_null($reservation)) {
            return response()
            ->json(['errors' => ['reservation_not_found']], 404);
        }

        return $reservation;
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'adult' => 'required|numeric',
            'child' => 'required|numeric',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'deposite' => 'nullable',
            'type' => 'required|in:personal,group',
            'status' => 'nullable',
            'guest' => 'required',
            'guest.name' => 'required',
            'guest.nip' => 'required|numeric',
            'guest.phone' => 'required|',
            'guest.email' => 'required|email',
            'guest.address' => 'required|',
            'guest.name_institusi' => 'required_if:type,group',
            'guest.user_id' => 'nullable|exists:users,id',
            'rooms' => 'required|array',
            'rooms.*' => 'required|exists:rooms,code',
            'payment' => 'required',
            'payment.information' => 'nullable',
            'payment.account_id' => 'required',
            'payment.account_type' => 'required|in:App\AccountBank,App\CreditCard',
        ]);

        if ($validation->fails()) {
            return response()
            ->json($validation->errors(), 422);
        }

        $user_data['rooms'] = $request['rooms'];
        $user_data['guest'] = $request['guest'];

        $user_data['payment'] = $request['payment'];
        $user_data['payment']['type'] = $request['type'];

        $user_data['reservation'] = $request->except(['guest', 'rooms', 'payment']);

        try {
            DB::transaction(function () use ($user_data, &$reservation) {
                $guest = Guest::create($user_data['guest']);

                $reservation = new Reservation($user_data['reservation']);
                $reservation->guest()->associate($guest);
                $reservation->save();
                $reservation->rooms()->sync($user_data['rooms']);

                app('App\Http\Controllers\PaymentController')->store(
                    $reservation->id,
                    'App\\Reservation',
                    $user_data['payment'],
                    $reservation->rooms
                );
            });

            return Reservation::with(['rooms', 'guest', 'payment', 'transaction'])->find($reservation->id);
        } catch(\Exception $e) {
            return response()
            ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    
    public function update(Request $request, $id)
    {
        $reservation = Reservation::find($id);

        if(is_null($reservation)) {
            return response()
            ->json(['errors' => ['reservation_not_found']], 404);
        }

        $validation = Validator::make($request->all(), [
            'adult' => 'sometimes|numeric',
            'child' => 'sometimes|numeric',
            'start_date' => 'sometimes|date',
            'end_date' => 'sometimes|date',
            'deposite' => 'nullable',
            'type' => 'sometimes|in:personal,group',
            'status' => 'nullable',
            'guest' => 'sometimes',
            'guest.name' => 'sometimes',
            'guest.nip' => 'sometimes|numeric',
            'guest.phone' => 'sometimes|',
            'guest.email' => 'sometimes|email',
            'guest.address' => 'sometimes|',
            'guest.name_institusi' => 'sometimes',
            'guest.user_id' => 'nullable|exists:users,id',
            'rooms' => 'sometimes|array',
            'rooms.*' => 'sometimes|exists:rooms,code',
            'payment' => 'sometimes',
            'payment.information' => 'sometimes',
            'payment.payment_date' => 'sometimes|boolean',
        ]);

        if ($validation->fails()) {
            return response()
            ->json($validation->errors(), 422);
        }

        $user_data['rooms'] = $request['rooms'];
        $user_data['guest'] = $request['guest'];
        $user_data['payment'] = $request['payment'];
        $user_data['reservation'] = $request->except(['guest', 'rooms', 'code']);

        try {
            DB::transaction(function () use ($user_data, &$reservation) {
                $reservation->update($user_data['reservation']);
                
                if ($user_data['guest'] != null) {
                    $reservation->guest()->update($user_data['guest']);
                }
                if (!is_null($user_data['rooms'])) {
                    $reservation->rooms()->sync($user_data['rooms']);
                    $reservation->save();
                }
                if (!is_null($user_data['payment'])) {
                    app('App\Http\Controllers\PaymentController')->update(
                    $user_data['payment'],
                    $reservation->payment['invoice']
                );
                }

            });

            return Reservation::with(['rooms', 'guest', 'payment', 'transaction'])->find($reservation->id);
        } catch(\Exception $e) {
            return response()
            ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reservation = Reservation::find($id);

        if(is_null($reservation)) {
            return response()
            ->json(['errors' => ['reservation_not_found']], 404);
        }

        $reservation->rooms()->detach();
        $reservation->delete();
        
        return response()->json();
    }
}
