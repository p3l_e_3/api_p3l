<?php

namespace App\Http\Controllers;

use App\BedType;
use Illuminate\Http\Request;

class BedTypeController extends Controller
{
    public function index()
    {
        return BedType::all();
    }
}
