<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    protected function getExpiredDate()
    {
        $date = date('Y-m-j');
        $newdate = strtotime ( '+7 day' , strtotime ( $date ) ) ;
        $newdate = date ( 'Y-m-j' , $newdate );
        return $newdate;
    }

    public function index()
    {
        return Payment::with(['account', 'payment_type'])->get();
    }

    public function store($payment_id, $payment_type, $payload, $optional = [])
    {
        $payment = new Payment;

        $payment->payment_type_id = $payment_id;
        $payment->payment_type_type = $payment_type;

        $payment->information = $payload['information'];
        $payment->expired_date = $this->getExpiredDate();
        
        if ($payload['account_type'] === 'App\\AccountBank') {
            $payment->account_type = $payload['account_type'];
            $payment->account_id = $payload['account_id'];
        } else if ($payload['account_type'] === 'App\\CreditCard') {
            $payment->account_type = $payload['account_type'];
            $payment->account_id = $payload['account_id'];
        }
        if ($payment_type === 'App\\Reservation') {
            $payment->typeReservation = $payload['type'];
            foreach ($optional as $item) {
                // dd($item->room_type->seasons); SEASON NON AKTIF
                $payment->amount += $item->room_type->price;
            }
        }
        
        if ($payment_type === 'App\\Transaction') {
            foreach ($optional as $item) {
                $payment->amount += $item->price;
            }
        }
        $payment->save();
    }

    public function update($payload, $id)
    {
        $payment = Payment::find($id);

        if(is_null($payment)) {
            return response()
            ->json(['errors' => ['reservation_not_found']], 404);
        }

        if(!is_null($payload['payment_date']) && $payload['payment_date'] == true) {
            $payload['payment_date'] = date('Y-m-d');
        }
        $payment->update($payload);
        $payment->save();
    }

    public function destroy($id)
    {
        $payment = Payment::with(['account', 'payment_type'])->find($id);

        if(is_null($payment)) {
            return response()
            ->json(['errors' => ['reservation_not_found']], 404);
        }

        $payment->delete();
        
        return response()->json();
    }
}
