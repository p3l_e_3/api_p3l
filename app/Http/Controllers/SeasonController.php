<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Season;
use Illuminate\Http\Request;

class SeasonController extends Controller
{
    public function index()
    {
        return Season::with(['room_types'])->get();
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'room_type' => 'sometimes|array',
            'room_type.*.room_type_code' => 'required|exists:room_types,code',
            'room_type.*.type' => 'required',
            'room_type.*.amount' => 'required|numeric'
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }
        $user_data['season'] = $request->except(['room_type']);
        $user_data['room_type'] = $request->room_type;
        
        try {
            DB::transaction(function () use ($user_data, &$season) {
                $season = Season::create($user_data['season']);

                if (!is_null($user_data['room_type'])) {
                    $season->room_types()->sync($user_data['room_type']);
                }
            });

            return Season::with(['room_types'])->find($season->id);
        } catch(\Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function update(Request $request, $id)
    {

        $season = Season::with(['room_types'])->find($id);

        if(is_null($season)) {
            return response()
                ->json(['errors' => ['season_not_found']], 404);
        }

        $validation = Validator::make($request->all(), [
            'name' => 'sometimes',
            'start_date' => 'sometimes|date',
            'end_date' => 'sometimes|date',
            'room_type' => 'sometimes|array',
            'room_type.*.room_type_code' => 'sometimes|exists:room_types,code',
            'room_type.*.type' => 'sometimes',
            'room_type.*.amount' => 'sometimes|numeric'
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }
        $user_data['season'] = $request->except(['room_type']);
        $user_data['room_type'] = $request->room_type;
        
        try {
            DB::transaction(function () use ($user_data, $season) {

                if (!is_null($user_data['season'])) {
                    $season->update($user_data['season']);
                }

                if (!is_null($user_data['room_type'])) {
                    $season->room_types()->sync($user_data['room_type']);
                }
            });

            return Season::with(['room_types'])->find($season->id);
        } catch(\Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function destroy($id)
    {
        $season = Season::with(['room_types'])->find($id);

        if(is_null($season)) {
            return response()
                ->json(['errors' => ['season_not_found']], 404);
        }
        
        $season->room_types()->detach();
        $season->delete();

        return response()->json();
    }
}
