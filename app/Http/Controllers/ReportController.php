<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function tamuTahunan()
    {   
        $query = 'SELECT MONTHNAME(created_at) as Bulan, count(id) as Jumlah FROM GUESTS GROUP BY MONTHNAME(created_at)';
        return DB::select($query);
    }

    public function pendapatanBulanan()
    {
        $query = 'SELECT
                    MONTHNAME(p.created_at) as Bulan, 
                    SUM(IF(r.type = "grup", p.amount, 0)) AS grup,
                    SUM(IF(r.type = "personal", p.amount, 0)) AS personal,
                    SUM(p.amount) as total
                    FROM payments as p 
                    JOIN reservations r ON r.id=p.payment_type_id
                    AND p.payment_type_type="App\\\Reservation"
                    WHERE p.payment_date IS NOT NULL
                    GROUP BY Bulan';

        return DB::select($query);
    }

    public function pendapatanTahunan()
    {
        $query = 'SELECT 
                    YEAR(trc.created_at) as Year, 
                    SUM(IF(rsv.type = "group", amount, 0)) AS Grup,
                    SUM(IF(rsv.type = "personal", amount, 0)) AS Personal,
                    SUM(amount) as Total
                    FROM payments trc 
                    JOIN reservations rsv ON trc.payment_type_id=rsv.id
                    AND trc.payment_type_type = "App\\\Reservation"
                    WHERE trc.payment_date IS NOT NULL
                    GROUP BY YEAR(trc.created_at) 
                    ORDER BY YEAR(trc.created_at)';

        return DB::select($query);
    }

    public function jumlahTamu()
    {
        $query = 'SELECT 
                    rtp.name as RmType, 
                    SUM(IF(r.type = "grup", 1, 0)) AS Grup, 
                    SUM(IF(r.type = "personal", 1, 0)) AS Personal,
                    COUNT(rmv.id) AS Total
                    FROM room_reservation rmv
                    JOIN reservations r ON rmv.reservation_id=r.id
                    JOIN rooms rm ON rmv.room_code=rm.code
                    JOIN room_types rtp ON rm.room_type_code=rtp.code
                    GROUP BY RmType
                    ORDER BY RmType';

        return DB::select($query);
    }

    public function top5Customer()
    {
        $query = 'SELECT 
                    gs.name "name", 
                    COUNT(rsv.id) "qty", 
                    SUM(p.amount) "amount"
                    FROM reservations rsv
                    JOIN payments p ON p.payment_type_id=rsv.id
                    AND p.payment_type_type = "App\\\Reservation"
                    JOIN guests gs ON rsv.guest_id=gs.id
                    WHERE p.payment_date IS NOT NULL
                    GROUP BY gs.name
                    ORDER BY COUNT(rsv.id)';
        return DB::select($query);
    }
}
