<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Branch;
use Illuminate\Http\Request;

class BranchController extends Controller
{
    public function index()
    {
        return Branch::all();
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required|unique:branches,name',
            'address' => 'required',
            'phone' => 'required',
        ]);

        if ($validation->fails()) {
            return response()
            ->json($validation->errors(), 422);
        }

        $user_data = $request->only('name', 'address', 'phone');

        try {
            DB::transaction(function () use ($user_data, &$branch) {
            $branch = Branch::create($user_data);
            });

            return Branch::find($branch->id);
        } catch(\Exception $e) {
            return response()
            ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function update(Request $request, $id)
    {
        $branch = Branch::find($id);

        if(is_null($branch)) {
            return response()
            ->json(['errors' => ['branch_not_found']], 404);
        }

        $validation = Validator::make($request->all(), [
            'name' => 'sometimes|unique:branches,name',
            'address' => 'sometimes',
            'phone' => 'sometimes',
        ]);

        if ($validation->fails()) {
            return response()
            ->json($validation->errors(), 422);
        }

        $user_data = $request->only('name', 'address', 'phone');

        try {
            DB::transaction(function () use ($user_data, $branch) {
            $branch->update($user_data);
            });

            return Branch::find($branch->id);
        } catch(\Exception $e) {
            return response()
            ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function destroy($id)
    {
        $branch = Branch::find($id);

        if(is_null($branch)) {
            return response()
            ->json(['errors' => ['branch_not_found']], 404);
        }

        $branch->delete();
        
        return response()->json();
    }
}
