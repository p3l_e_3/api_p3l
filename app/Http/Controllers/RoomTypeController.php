<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\RoomType;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class RoomTypeController extends Controller
{
    public function index()
    {
        $room_type =  RoomType::with(['facilities', 'bed_types', 'seasons', 'images'])->get();
        $collection = collect($room_type);
        $collection->each(function ($item, $key) {
            foreach ($item->images as $value) {
                $value->url = 'http://127.0.0.1:8000/storage/images/'.$value->file_name;
            }
        });

        return $collection->all();
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'code' => 'required|unique:room_types,code',
            'name' => 'required|unique:room_types,name',
            'price' => 'required|numeric',
            'max_guests' => 'required|numeric|max:25',
            'size' => 'required|numeric',
            'description' => 'required',
            'facilities' => 'required|array',
            'facilities.*' => 'integer|exists:facilities,id',
            'bed_types' => 'required|array',
            'bed_types.*.bed_type_id' => 'integer|exists:bed_types,id',
            'bed_types.*.qty' => 'integer|max:5'
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }
        
        $user_data['room_type'] = $request->except(['facilities','bed_types']);
        $user_data['facilities'] = $request->facilities;
        $user_data['bed_types'] = $request->bed_types;

        try {
            DB::transaction(function () use ($user_data, &$room_type) {
                $room_type = RoomType::create($user_data['room_type']);
                $room_type->facilities()->sync($user_data['facilities']);
                $room_type->bed_types()->sync($user_data['bed_types']);
            });

            return RoomType::with(['facilities', 'bed_types'])->find($room_type->code);
        } catch (\Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function update(Request $request, $code)
    {
        $room_type = RoomType::with(['facilities', 'bed_types'])->find($code);

        if(is_null($room_type)) {
            return response()
                ->json(['errors' => ['room_type_not_found']], 404);
        }

        $validation = Validator::make($request->all(), [
            'price' => 'sometimes|numeric',
            'max_guests' => 'sometimes|numeric|max:25',
            'size' => 'sometimes|numeric',
            'description' => 'sometimes',
            'facilities' => 'sometimes|array',
            'facilities.*' => 'exists:facilities,id',
            'bed_types' => 'sometimes|array',
            'bed_types.*.bed_type_id' => 'integer|exists:bed_types,id',
            'bed_types.*.qty' => 'integer|max:5'
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }

        $user_data['room_type'] = $request->except(['facilities','bed_types']);
        $user_data['facilities'] = $request->facilities;
        $user_data['bed_types'] = $request->bed_types;

        try {
            DB::transaction(function () use ($user_data, $room_type) {
                if (!is_null($user_data['room_type'])) {
                    $room_type->update($user_data['room_type']);
                }
                
                if (!is_null($user_data['facilities'])) {
                    $room_type->facilities()->sync($user_data['facilities']);
                }

                if (!is_null($user_data['bed_types'])) {
                    $room_type->bed_types()->sync($user_data['bed_types']);
                }
            });

            return RoomType::with(['facilities', 'bed_types'])->find($room_type->code);
        } catch (\Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function destroy($id)
    {
        $room_type = RoomType::with(['facilities', 'bed_types', 'seasons'])->find($id);

        if(is_null($room_type)) {
            return response()
                ->json(['errors' => ['room_type_not_found']], 404);
        }

        $room_type->facilities()->detach();
        $room_type->bed_types()->detach();
        $room_type->delete();

        return response()->json();
    }

    public function addImages(Request $request, $id)
    {
        $room_type = RoomType::find($id);

        if(is_null($room_type)) {
            return response()
                ->json(['errors' => ['room_type_not_found']], 404);
        }

        $validation = Validator::make($request->all(), [
            'images' => 'sometimes|array',
            'images.*' => 'exists:images,id',
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }

        $user_data = $request['images'];

        $room_type->images()->sync($user_data);

        return  $room_type->images()->get(); 
    }
}
