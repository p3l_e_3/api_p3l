<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\CreditCard;
use Illuminate\Http\Request;

class CreditCardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return CreditCard::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'account_number' => 'required|numeric|digits:15|unique:credit_cards,account_number',
            'account_name' => 'required',
            'type' => 'required|in:master_card,visa',
            'expiry' => 'required|date_format:y-m',
            'vcc' => 'required|numeric|digits:3',
        ]);

        if ($validation->fails()) {
            return response()
            ->json($validation->errors(), 422);
        }

        $user_data = $request->all();

        try {
            DB::transaction(function () use ($user_data, &$credit_card) {
            $credit_card = CreditCard::create($user_data);
            });

            return CreditCard::find($credit_card->account_number);
        } catch(\Exception $e) {
            return response()
            ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CreditCard  $creditCard
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $credit_card = CreditCard::find($id);

        if(is_null($credit_card)) {
            return response()
            ->json(['errors' => ['credit_card_not_found']], 404);
        }

        $validation = Validator::make($request->all(), [
            'account_name' => 'sometimes',
            'type' => 'sometimes|in:master_card,visa',
            'expiry' => 'sometimes|date_format:y-m',
            'vcc' => 'sometimes|numeric|digits:3',
        ]);

        if ($validation->fails()) {
            return response()
            ->json($validation->errors(), 422);
        }

        $user_data = $request->only('account_name', 'type', 'expiry', 'vcc');

        try {
            DB::transaction(function () use ($user_data, $credit_card) {
            $credit_card->update($user_data);
            });

            return CreditCard::find($credit_card->account_number);
        } catch(\Exception $e) {
            return response()
            ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CreditCard  $creditCard
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $credit_card = CreditCard::find($id);

        if(is_null($credit_card)) {
            return response()
            ->json(['errors' => ['credit_card_not_found']], 404);
        }

        $credit_card->delete();
        
        return response()->json();
    }
}
