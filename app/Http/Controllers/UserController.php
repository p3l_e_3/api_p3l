<?php

namespace App\Http\Controllers;

use App\User;
use Validator;
use DB;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::with('role')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'username' => 'required|unique:users,username',
            'password' => 'required',
            'email' => 'required|unique:users,email',
            'role_id' => 'required|exists:roles,id',
        ]);

        if ($validation->fails()) {
            return response()
            ->json($validation->errors(), 422);
        }

        $user_data['user'] = $request->except(['password']);
        $user_data['user']['password'] = bcrypt($request->password);

        try {
            DB::transaction(function () use ($user_data, &$user) {
            $user = User::create($user_data['user']);
            });

            return User::with('role')->find($user->id);
        } catch(\Exception $e) {
            return response()
            ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::with(['role'])->find($id);

        if(is_null($user)) {
            return response()
            ->json(['errors' => ['user_not_found']], 404);
        }

        $validation = Validator::make($request->all(), [
            'password' => 'sometimes',
            'email' => 'sometimes|unique:users,email',
            'role_id' => 'sometimes|exists:roles,id',
        ]);
        
        if ($validation->fails()) {
            return response()
            ->json($validation->errors(), 422);
        }

        $user_data['user'] = $request->only(['email', 'role_id']);

        if (!is_null($request->password)) {
            $user_data['user']['password'] = bcrypt($request->password);
        }

        try {
            DB::transaction(function () use ($user_data, $user) {
                $user->update($user_data['user']);
            });

            return User::with('role')->find($user->id);
        } catch (\Exception $e) {
            return response()
            ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
      
        if(is_null($user)) {
            return response()
            ->json(['errors' => ['user_not_found']], 404);
        }

        $user->delete();
        
        return response()->json();
    
    }
}
