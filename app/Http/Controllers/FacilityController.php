<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Facility;
use Illuminate\Http\Request;

class FacilityController extends Controller
{
    public function index() {
        return Facility::all();
    }

    public function store(Request $request) {
        $validation = Validator::make($request->all(), [
            'name' => 'required|unique:facilities,name',
        ]);

        if ($validation->fails()) {
            return response()
            ->json($validation->errors(), 422);
        }

        $user_data = $request->only('name');

        try {
            DB::transaction(function () use ($user_data, &$facility) {
            $facility = Facility::create($user_data);
            });

            return Facility::find($facility->id);
        } catch(\Exception $e) {
            return response()
            ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function update(Request $request, $id) {
        $facility = Facility::find($id);

        if(is_null($facility)) {
            return response()
            ->json(['errors' => ['facility_not_found']], 404);
        }

        $validation = Validator::make($request->all(), [
            'name' => 'sometimes|unique:facilities,name',
        ]);

        if ($validation->fails()) {
            return response()
            ->json($validation->errors(), 422);
        }

        $user_data = $request->only('name');

        try {
            DB::transaction(function () use ($user_data, $facility) {
            $facility->update($user_data);
            });

            return Facility::find($facility->id);
        } catch(\Exception $e) {
            return response()
            ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function destroy($id) {
        $facility = Facility::find($id);

        if(is_null($facility)) {
            return response()
            ->json(['errors' => ['facility_not_found']], 404);
        }

        $facility->delete();
        
        return response()->json();
    }
}
