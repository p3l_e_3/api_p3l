<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Room;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

class RoomController extends Controller
{

    public function index()
    {
        return Room::with([
            'room_type', 
            'room_type.bed_types', 
            'room_type.facilities', 
            'room_type.seasons', 
            'room_bed', 
            'room_bed.bed_type', 
            'branch',
            'reservation'])
            ->get();
    }

    public function indexAvailable(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'start_date' => 'required|date',
            'end_date' => 'required|date',
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }
        
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');

        $room = Room::with([
            'room_type',
            'room_type.bed_types',
            'room_type.facilities', 
            'room_type.seasons', 
            'room_type.images', 
            'room_bed', 
            'room_bed.bed_type',
            'branch'
            ])
            ->get();

        $room->load([
            'reservation' => function($query) use ($start_date, $end_date) {
                $query
                    ->whereBetween('start_date', [$start_date, $end_date])
                    ->orWhereBetween('end_date', [$start_date, $end_date]);
            }
        ]);

        
        $collection = collect($room);
        $collection->filter(function ($item, $key) {
            return $item->reservation->isEmpty();          
        });
        
        return $collection;
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'floor' => 'required|numeric',
            'number' => 'required|numeric',
            'smoking' => 'required|boolean',
            'status' => 'nullable|boolean',
            'room_type_code' => 'required|exists:room_types,code',
            'branch_id' => 'required|integer|exists:branches,id',
            'room_bed_id' => 'required|integer|exists:bed_type_room_type,id',
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }
        
        $user_data = $request->all();

        try {
            DB::transaction(function () use ($user_data, &$room) {
                $room = Room::create($user_data);
            });

            return Room::with(['room_type', 'branch', 'room_bed'])->find($room->code);
        } catch(\Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function update(Request $request, $code)
    {
        $room = Room::find($code);

        if(is_null($room)) {
            return response()
                ->json(['errors' => ['room_not_found']], 404);
        }

        $validation = Validator::make($request->all(), [
            'smoking' => 'sometimes|boolean',
            'status' => 'sometimes|boolean',
            'branch_id' => 'sometimes|integer|exists:branches,id',
            'room_bed_id' => 'sometimes|integer|exists:bed_type_room_type,id',
        ]);

        if ($validation->fails()) {
            return response()
                ->json($validation->errors(), 422);
        }

        $user_data = $request->all();

        try {
            DB::transaction(function () use ($user_data, $room) {
                $room->update($user_data);
            });

            return Room::with(['room_type', 'branch', 'room_bed'])->find($room->code);
        } catch (\Exception $e) {
            return response()
                ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function destroy($code)
    {
        $room = Room::find($code);

        if(is_null($room)) {
            return response()
                ->json(['errors' => ['room_not_found']], 404);
        }

        $room->delete();

        return response()->json();
    }
}
