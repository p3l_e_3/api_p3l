<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\PaidFacility;
use Illuminate\Http\Request;

class PaidFacilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PaidFacility::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required|unique:paid_facilities,name',
            'price' => 'required|numeric',
        ]);

        if ($validation->fails()) {
            return response()
            ->json($validation->errors(), 422);
        }

        $user_data = $request->only('name', 'price');

        try {
            DB::transaction(function () use ($user_data, &$paid_facility) {
            $paid_facility = PaidFacility::create($user_data);
            });

            return PaidFacility::find($paid_facility->id);
        } catch(\Exception $e) {
            return response()
            ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PaidFacility  $paidFacility
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $paid_facility = PaidFacility::find($id);

        if(is_null($paid_facility)) {
            return response()
            ->json(['errors' => ['paid_facility_not_found']], 404);
        }

        $validation = Validator::make($request->all(), [
            'name' => 'sometimes|unique:facilities,name',
            'price' => 'required|numeric',
        ]);

        if ($validation->fails()) {
            return response()
            ->json($validation->errors(), 422);
        }

        $user_data = $request->only('name', 'price');

        try {
            DB::transaction(function () use ($user_data, $paid_facility) {
            $paid_facility->update($user_data);
            });

            return PaidFacility::find($paid_facility->id);
        } catch(\Exception $e) {
            return response()
            ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PaidFacility  $paidFacility
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $paid_facility = PaidFacility::find($id);

        if(is_null($paid_facility)) {
            return response()
            ->json(['errors' => ['paid_facility_not_found']], 404);
        }

        $paid_facility->delete();
        
        return response()->json();
    }
}
