<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function index()
    {
        return Transaction::with(['reservation', 'facilities', 'payment'])->get();
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'reservation_id' => 'required|exists:reservations,id|unique:transactions,reservation_id',
            'facilities' => 'nullable|array',
            'facilities.*' => 'nullable|exists:paid_facilities,id'
        ]);

        if ($validation->fails()) {
            return response()
            ->json($validation->errors(), 422);
        }

        
        $user_data = $request->only('reservation_id');
        $user_data['facilities'] = $request['facilities'];
        $user_data['check_in'] = date('Y-m-j');
        try {
            DB::transaction(function () use ($user_data, &$transaction) {
                $transaction = Transaction::create($user_data);

                if (!is_null($user_data['facilities'])) {
                    $transaction->facilities()->sync($user_data['facilities']);
                }

            });

            return Transaction::with(['reservation', 'facilities', 'payment'])->find($transaction->id);
        } catch(\Exception $e) {
            return response()
            ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function update(Request $request, $id)
    {
        $transaction = Transaction::find($id);

        if(is_null($transaction)) {
            return response()
            ->json(['errors' => ['transaction_not_found']], 404);
        }

        $validation = Validator::make($request->all(), [
            'check_out' => 'sometimes|boolean',
            'payment' => 'required_if:check_out,1',
            'payment.information' => '',
            'payment.account_id' => '',
            'payment.account_type' => 'in:App\\AccountBank,App\\CreditCard'
        ]);

        if ($validation->fails()) {
            return response()
            ->json($validation->errors(), 422);
        }

        $user_data['check_out'] = $request['check_out'] == true ? date('Y-m-d') : null;
        $user_data['facilities'] = $request['facilities'];
        $user_data['payment'] = $request['payment'];

        try {
            DB::transaction(function () use ($user_data, $transaction) {
            $transaction->update($user_data);
                // if (!is_null($user_data['facilities'])) {
                    // $transaction->facilities()->sync($user_data['facilities']);
                // }

                if (!is_null($user_data['payment'])) {
                    app('App\Http\Controllers\PaymentController')->store(
                        $transaction->id,
                        'App\\Transaction',
                        $user_data['payment'],
                        $transaction->facilities
                    );
                }

            });

            return Transaction::with(['reservation', 'facilities', 'payment'])->find($transaction->id);
        } catch(\Exception $e) {
            return response()
            ->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    public function destroy($id)
    {
        $transaction = Transaction::find($id);

        if(is_null($transaction)) {
            return response()
            ->json(['errors' => ['transaction_not_found']], 404);
        }

        $transaction->delete();
        
        return response()->json();
    }
}
