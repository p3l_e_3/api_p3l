<?php

namespace App\Http\Controllers;

use DB;
use Storage;
use Validator;
use App\Image;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    public function index()
    {
        return Image::all();
    }
    
    public function get($id)
    {
        $image = Image::find($id);
        $image->url = $image->url();

        if (is_null($image)) {
            return response()
                ->json(['errors' => ['image_not_found']], 404);
        }

        return $image;
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'image' => 'required|file|mimes:jpeg,jpg,png,gif'
        ]);

        if ($validation->fails()) {
            return response()->json(['errors' => $validation->errors()], 422);
        }

        $files = $request->files->get('image');

        try{
            DB::transaction(function () use ($files, &$image) {
                $file_name = Image::sanitizeFileName(
                    $files->getClientOriginalName(),
                    $files->getClientOriginalExtension()
                );

                Storage::putFileAs('public/images', $files, $file_name);
                
                $image = Image::create([
                    'file_name' => $file_name,
                    'extension' => $files->getClientOriginalExtension(),
                    'size' => $files->getClientSize()
                ]);

            });

            return $image;
        } catch(\Exception $e) {
            return response()->json(['errors' => [$e->getMessage()]], 500);
        }
    }
}
