<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reservation extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'code',
        'adult',
        'child',
        'start_date',
        'end_date',
        'deposite',
        'type',
        'status',
        'guest_id',
    ];

    protected $hidden = [
        'guest_id',
    ];


    // RELATION

    public function guest() {
        return $this->belongsTo('App\Guest');
    }

    public function payment() {
        return $this->morphOne('App\Payment', 'payment_type');
    }

    public function transaction() {
        return $this->hasOne('App\Transaction');
    }

    public function rooms() {
        return $this->belongsToMany('App\Room', 'room_reservation')->with('room_type', 'room_type.seasons');
    }

    // STATIC
    
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model)
        {
            $model->incrementCode();
        });
    }

    protected function incrementCode() {
        $this->code = strtoupper(substr($this->type, 0, 1)).date('ymd').'-'.str_pad($this->lastId(), 3, '0', STR_PAD_LEFT);
    }

    // EXTERNAL

    protected function lastId() {
        $booking = $this->withTrashed()->get()->last();

        if(is_null($booking)) {
            return 1;
        }

        return substr($booking->code, -3) + 1;
    }
    

}
