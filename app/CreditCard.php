<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditCard extends Model
{
    public $primaryKey = 'account_number';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'account_number',
        'account_name',
        'type',
        'expiry',
        'vcc',
    ];
}
