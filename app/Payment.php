<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public $primaryKey = 'invoice';
    public $incrementing = false;

    protected $fillable = [
        'invoice',
        'amount',
        'expired_date',
        'payment_date',
        'information',
        'account_id',
        'account_type',
    ];

    // protected $hidden = [
    //     'account_id',
    //     'account_type',
    // ];

    public function account()
    {
        return $this->morphTo();
    }

    public function payment_type()
    {
        return $this->morphTo();
    }

    // STATIC

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model)
        {
            $model->incrementCode();
        });
    }
    
    protected function incrementCode() {
        $type = $this->typeReservation;
        $lastId = $this->lastId();
        unset($this->typeReservation);
        
        $this->invoice = strtoupper(substr($type, 0, 1)).date('ymd').'-'.str_pad($lastId, 3, '0', STR_PAD_LEFT);
    }


    // EXTERNAL

    protected function lastId() {
        $payment = $this->get()->last();

        if(is_null($payment)) {
            return 1;
        }

        return substr($payment->invoice, -3) + 1;
    }
}
