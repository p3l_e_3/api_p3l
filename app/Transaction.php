<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'check_in',
        'check_out',
        'reservation_id',
    ];

    protected $hidden = [
    ];

    public function facilities()
    {
        return $this->belongsToMany('App\PaidFacility');
    }

    public function reservation()
    {
        return $this->belongsTo('App\Reservation');
    }

    public function payment()
    {
        return $this->belongsTo('App\Payment');
    }
}
