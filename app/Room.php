<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    public $primaryKey = 'code';
    
    public $incrementing = false;

    protected $fillable = [
        'code',
        'floor',
        'number',
        'smoking',
        'status',
        'room_type_code',
        'branch_id',
        'room_bed_id'
    ];

    protected $hidden = [
        'room_type_code',
        'branch_id',
        'room_bed_id'
    ];

    public function branch() {
        return $this->belongsTo('App\Branch');
    }

    public function room_type() {
        return $this->belongsTo('App\RoomType');
    }

    public function room_bed() {
        return $this->belongsTo('App\BedRoom', 'room_bed_id');
    }
    
    public function reservation() {
        return $this->belongsToMany('App\Reservation', 'room_reservation');
    }
    
    // STATIC

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model)
        {
            $model->setPrimaryKey();
        });
    }

    protected function setPrimaryKey()
    {
        $user_data['floor'] = sprintf("%02d", $this->floor);
        $user_data['number'] = sprintf("%02d", $this->number);
        $user_data['room_type'] = $this->room_type_code;
        
        $this->code = implode($user_data);
    }

}
