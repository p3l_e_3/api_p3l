<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomType extends Model
{
    public $timestamps = false;

    public $primaryKey = 'code';
    
    public $incrementing = false;
    
    protected $fillable = [
        'code',
        'name',
        'price',
        'max_guests',
        'size',
        'description'
    ];

    public function facilities() {
        return $this->belongsToMany('App\Facility');
    }

    public function bed_types() {
        return $this->belongsToMany('App\BedType')->withPivot('id', 'qty');
    }   

    public function seasons() {
        return $this->belongsToMany('App\Season')->withPivot('type', 'amount');
    }

    public function images() {
        return $this->belongsToMany('App\Image');
    }
}
