<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class BedRoom extends Pivot
{
    protected $table = 'bed_type_room_type';

    protected $hidden = [
        'bed_type_id',
        'room_type_code'
    ];

    public function bed_type() {
        return $this->belongsTo('App\BedType');
    }
}