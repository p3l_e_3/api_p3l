<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountBank extends Model
{
    public $primaryKey = 'account_number';
    public $incrementing = false;
    public $timestamps = false;
    
    protected $fillable = [
        'account_number',
        'type',
        'account_name',
    ];

    public function payment()
    {
        return $this->morphOne('App\Payment', 'account');
    }
}
