<?php

use Faker\Generator as Faker;

$factory->define(App\Guest::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName($gender = null|'male'|'female'),
        'nip' => $faker->randomNumber($nbDigits = 15, $strict = false),
        'phone' => $faker->tollFreePhoneNumber,
        'email' => $faker->safeEmail,
        'address' => $faker->address,
        'name_institusi' => $faker->streetName,
        'user_id' => $faker->numberBetween($min = 1, $max = 25)
    ];
});
