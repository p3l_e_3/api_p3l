<?php

use Faker\Generator as Faker;

$factory->define(App\PaidFacility::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'price' => $faker->numberBetween($min = 20000, $max = 70000)
    ];
});
