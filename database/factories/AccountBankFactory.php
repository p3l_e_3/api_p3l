<?php

use Faker\Generator as Faker;

$factory->define(App\AccountBank::class, function (Faker $faker) {
    return [
        'account_number' => substr($faker->unique()->creditCardNumber,0 , 15),
        'type' => $faker->randomElement($array = array ('bca','bni','bri', 'cimb_niaga')),
        'account_name' => $faker->lastName,
    ];
});
