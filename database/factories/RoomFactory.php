<?php

use Faker\Generator as Faker;

$factory->define(App\Room::class, function (Faker $faker) {
    return [
        'floor' => $faker->unique()->randomNumber(2),
        'number' => $faker->unique()->randomNumber(2),
        'smoking' => $faker->randomElement($array = array (True, False)),
        'room_type_code' => $faker->randomElement($array = array ('SR','DD','ER', 'JR')),
        'branch_id' => $faker->randomElement($array = array (1, 2)),
        'room_bed_id' => $faker->randomElement($array = array (1, 2, 3)),
    ];
});
