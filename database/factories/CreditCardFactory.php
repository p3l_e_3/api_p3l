<?php

use Faker\Generator as Faker;

$factory->define(App\CreditCard::class, function (Faker $faker) {
    return [
        'account_number' => substr($faker->unique()->creditCardNumber,0 , 15),
        'account_name' => $faker->lastName,
        'type' => $faker->randomElement($array = array ('master_card','visa')),
        'expiry' => $faker->creditCardExpirationDate,
        'vcc' => $faker->randomNumber($nbDigits = 3, $strict = false),
    ];
});
