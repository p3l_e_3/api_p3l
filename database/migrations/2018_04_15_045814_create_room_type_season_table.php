<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomTypeSeasonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_type_season', function (Blueprint $table) {
            $table->increments('id');
            $table->string('room_type_code');
            $table->unsignedInteger('season_id');
            $table->enum('type', ['high', 'low']);
            $table->double('amount');
            $table->timestamps();

            $table->foreign('room_type_code')->references('code')->on('room_types');
            $table->foreign('season_id')->references('id')->on('seasons');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_type_season');
    }
}
