<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaidFacilityTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paid_facility_transaction', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('paid_facility_id');
            $table->unsignedInteger('transaction_id');

            $table->foreign('paid_facility_id')->references('id')->on('paid_facilities');
            $table->foreign('transaction_id')->references('id')->on('transactions');
 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paid_facility_transaction');
    }
}
