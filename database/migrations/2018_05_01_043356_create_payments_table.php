<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->string('invoice')->unique()->index();
            $table->double('amount');
            $table->date('expired_date');
            $table->date('payment_date')->nullable();
            $table->text('information')->nullable();
            $table->morphs('account');
            $table->morphs('payment_type');
            $table->timestamps();
            $table->softDeletes();
            
            $table->primary('invoice');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
