<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->string('code');
            $table->integer('floor');
            $table->integer('number');
            $table->boolean('smoking');
            $table->string('room_type_code');
            $table->unsignedInteger('branch_id');
            $table->unsignedInteger('room_bed_id')->nullable();
            $table->timestamps();

            $table->primary('code');
            
            $table->foreign('room_type_code')->references('code')->on('room_types');
            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('room_bed_id')->references('id')->on('bed_type_room_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
