<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique()->index();
            $table->unsignedDecimal('adult', 2, 0);
            $table->unsignedDecimal('child', 2, 0);
            $table->date('start_date');
            $table->date('end_date');
            $table->boolean('deposite')->default(false);
            $table->enum('type', array('personal', 'group'));
            $table->enum('status', array('waiting', 'onprocess', 'complete'))->default('waiting');
            $table->unsignedInteger('guest_id');
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('guest_id')->references('id')->on('guests');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
