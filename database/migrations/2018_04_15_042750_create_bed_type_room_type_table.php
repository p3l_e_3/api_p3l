<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBedTypeRoomTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bed_type_room_type', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('bed_type_id');
            $table->string('room_type_code');
            $table->unsignedInteger('qty');

            $table->foreign('bed_type_id')->references('id')->on('bed_types');
            $table->foreign('room_type_code')->references('code')->on('room_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bed_type_room_type');
    }
}
