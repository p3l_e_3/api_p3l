<?php

use App\Branch;
use Illuminate\Database\Seeder;

class BranchTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Branch::create([
            'name' => 'Yogyakarta',
            'address' => 'Jln Kaliurang KM 15',
            'phone' => '(0274) 8811223',
        ]);

        Branch::create([
            'name' => 'Bandung',
            'address' => 'Jln Babarsari Z9',
            'phone' => '(021) 8844321',
        ]);
    }
}
