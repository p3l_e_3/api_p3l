<?php

use Illuminate\Database\Seeder;

use App\Image;
class MediaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Image::create([
            'file_name' => 'parallax-10.jpg',
            'extension' => 'jpg',
            'size' => '10000'
        ]);
        Image::create([
            'file_name' => 'img-1-1024x963',
            'extension' => 'jpg',
            'size' => '10000'
        ]);
        Image::create([
            'file_name' => 'img1',
            'extension' => 'jpg',
            'size' => '10000'
        ]);
        Image::create([
            'file_name' => 'img2',
            'extension' => 'jpg',
            'size' => '10000'
        ]);
        Image::create([
            'file_name' => 'parallax-4',
            'extension' => 'jpg',
            'size' => '10000'
        ]);
        Image::create([
            'file_name' => 'parallax-5',
            'extension' => 'jpg',
            'size' => '10000'
        ]);
        Image::create([
            'file_name' => 'parallax-6',
            'extension' => 'jpg',
            'size' => '10000'
        ]);
    }
}
