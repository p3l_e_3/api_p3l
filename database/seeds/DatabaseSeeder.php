<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(BedTypesTableSeeder::class);
        $this->call(BranchTableSeeder::class);
        $this->call(FacilitiesTableSeeder::class);
        $this->call(PaidFacilitiesTableSeeder::class);
        $this->call(RoomTypesTableSeeder::class);
        $this->call(RoomsTableSeeder::class);
        $this->call(MediaTableSeeder::class);
        $this->call(AccountBankTableSeeder::class);
        $this->call(CreditCardTableSeeder::class);
    }
}
