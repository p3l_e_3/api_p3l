<?php

use App\AccountBank;
use Illuminate\Database\Seeder;

class AccountBankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(AccountBank::class, 7)->create();
    }
}
