<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'username' => 'arthadede',
            'password' => Hash::make('admin123'),
            'email' => 'x4.artha@yahoo.com',
            'role_id' => 1
        ]);
        User::create([
            'username' => 'admin',
            'password' => Hash::make('admin'),
            'email' => 'x.artha@yahoo.com',
            'role_id' => 1
        ]);
        User::create([
            'username' => 'frontoffice',
            'password' => Hash::make('frontoffice'),
            'email' => 'x1.artha@yahoo.com',
            'role_id' => 2
        ]);
        User::create([
            'username' => 'user',
            'password' => Hash::make('user'),
            'email' => 'x2.artha@yahoo.com',
            'role_id' => 3
        ]);

        factory(User::class, 50)->create();
    }
}
