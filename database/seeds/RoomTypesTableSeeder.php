<?php

use App\RoomType;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class RoomTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();

        $room_type = new RoomType;
        $room_type->code = 'SR';
        $room_type->name = 'Superior Room';
        $room_type->price = 600000;
        $room_type->size = 15;
        $room_type->max_guests = 3;
        $room_type->description = $faker->text($maxNbChars = 200);
        $room_type->save();
        $values = array();
        for ($i=0; $i < 10; $i++) {
        $values []= $faker->numberBetween($min = 1, $max = 20);
        }
        $room_type->facilities()->sync($values);
        $room_type->bed_types()->sync([['bed_type_id' => 3, 'qty' => 1], ['bed_type_id' => 2, 'qty' => 1]]);

        $room_type = new RoomType;
        $room_type->code = 'DD';
        $room_type->name = 'Double Deluxe Room';
        $room_type->price = 300000;
        $room_type->size = 15;
        $room_type->max_guests = 3;
        $room_type->description = $faker->text($maxNbChars = 200);
        $room_type->save();
        for ($i=0; $i < 5; $i++) {
        $values []= $faker->numberBetween($min = 1, $max = 20);
        }
        $room_type->facilities()->sync($values);
        $room_type->bed_types()->sync([['bed_type_id' => 3, 'qty' => 2], ['bed_type_id' => 2, 'qty' => 1]]);

        $room_type = new RoomType;
        $room_type->code = 'ER';
        $room_type->name = 'Executive Room';
        $room_type->price = 900000;
        $room_type->size = 15;
        $room_type->max_guests = 3;
        $room_type->description = $faker->text($maxNbChars = 200);
        $room_type->save();
        for ($i=0; $i < 5; $i++) {
        $values []= $faker->numberBetween($min = 1, $max = 20);
        }
        $room_type->facilities()->sync($values);
        $room_type->bed_types()->sync([['bed_type_id' => 1, 'qty' => 1]]);

        $room_type = new RoomType;
        $room_type->code = 'JR';
        $room_type->name = 'Junior Room';
        $room_type->price = 1000000;
        $room_type->size = 15;
        $room_type->max_guests = 3;
        $room_type->description = $faker->text($maxNbChars = 200);
        $room_type->save();
        for ($i=0; $i < 5; $i++) {
        $values []= $faker->numberBetween($min = 1, $max = 20);
        }
        $room_type->facilities()->sync($values);
        $room_type->bed_types()->sync([['bed_type_id' => 1, 'qty' => 1]]);

    }
}
