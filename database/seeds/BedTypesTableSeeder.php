<?php

use App\BedType;
use Illuminate\Database\Seeder;

class BedTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BedType::create([
            'name' => 'King'
        ]);

        BedType::create([
            'name' => 'Double'
        ]);

        BedType::create([
            'name' => 'Twin'
        ]);
    }
}
