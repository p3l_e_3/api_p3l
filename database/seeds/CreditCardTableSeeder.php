<?php

use App\CreditCard;
use Illuminate\Database\Seeder;

class CreditCardTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(CreditCard::class, 4)->create();
    }
}
