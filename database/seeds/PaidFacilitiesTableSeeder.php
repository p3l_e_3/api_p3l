<?php

use App\PaidFacility;
use Illuminate\Database\Seeder;

class PaidFacilitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(PaidFacility::class, 20)->create();
    }
}
