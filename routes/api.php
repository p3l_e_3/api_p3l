<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'AuthController@login');

Route::post('room/available', 'RoomController@indexAvailable');

Route::group(['middleware' => ['jwt.auth']], function() {
    Route::get('info', 'AuthController@me');
    Route::post('logout', 'AuthController@logout');

    Route::get('role', 'RoleController@index');
    Route::get('branch', 'BranchController@index');
    Route::get('bed_type', 'BedTypeController@index');
    Route::get('guest', 'GuestController@index');

    Route::group(['prefix' => 'image'], function() {
        Route::get('/', 'ImageController@index');
        Route::get('/{id}', 'ImageController@get');
        Route::post('/', 'ImageController@store');
    });

    Route::group(['prefix' => 'user'], function() {
        Route::get('/', 'UserController@index');
        Route::post('/', 'UserController@store');
        Route::patch('/{id}', 'UserController@update');
        Route::delete('/{id}', 'UserController@destroy');
    });

    Route::group(['prefix' => 'facility'], function() {
        Route::get('/', 'FacilityController@index');
        Route::post('/', 'FacilityController@store');
        Route::patch('/{id}', 'FacilityController@update');
        Route::delete('/{id}', 'FacilityController@destroy');
    });

    Route::group(['prefix' => 'room_type'], function() {
        Route::get('/', 'RoomTypeController@index');
        Route::post('/', 'RoomTypeController@store');
        Route::patch('/{code}', 'RoomTypeController@update');
        Route::delete('/{code}', 'RoomTypeController@destroy');
        Route::patch('/{code}/image', 'RoomTypeController@addImages');
    });

    Route::group(['prefix' => 'room'], function() {
        Route::get('/', 'RoomController@index');
        Route::post('/', 'RoomController@store');
        Route::patch('/{code}', 'RoomController@update');
        Route::delete('/{code}', 'RoomController@destroy');
    });

    Route::group(['prefix' => 'season'], function() {
        Route::get('/', 'SeasonController@index');
        Route::post('/', 'SeasonController@store');
        Route::patch('/{id}', 'SeasonController@update');
        Route::delete('/{id}', 'SeasonController@destroy');
    });

    Route::group(['prefix' => 'paid_facility'], function() {
        Route::get('/', 'PaidFacilityController@index');
        Route::post('/', 'PaidFacilityController@store');
        Route::patch('/{id}', 'PaidFacilityController@update');
        Route::delete('/{id}', 'PaidFacilityController@destroy');
    });

    Route::group(['prefix' => 'credit_card'], function() {
        Route::get('/', 'CreditCardController@index');
        Route::post('/', 'CreditCardController@store');
        Route::patch('/{id}', 'CreditCardController@update');
        Route::delete('/{id}', 'CreditCardController@destroy');
    });

    Route::group(['prefix' => 'account_bank'], function() {
        Route::get('/', 'AccountBankController@index');
        Route::post('/', 'AccountBankController@store');
        Route::patch('/{id}', 'AccountBankController@update');
        Route::delete('/{id}', 'AccountBankController@destroy');
    });

    Route::group(['prefix' => 'reservation'], function() {
        Route::get('/', 'ReservationController@index');
        Route::get('/{id}', 'ReservationController@get');
        Route::post('/', 'ReservationController@store');
        Route::patch('/{id}', 'ReservationController@update');
        Route::delete('/{id}', 'ReservationController@destroy');
    });

    Route::group(['prefix' => 'payment'], function() {
        Route::get('/', 'PaymentController@index');
        Route::patch('/{id}', 'PaymentController@update');
        Route::delete('/{id}', 'PaymentController@destroy');
    });

    Route::group(['prefix' => 'transaction'], function() {
        Route::get('/', 'TransactionController@index');
        Route::post('/', 'TransactionController@store');
        Route::patch('/{id}', 'TransactionController@update');
        Route::delete('/{id}', 'TransactionController@destroy');
    });

    Route::group(['prefix' => 'reports'], function() {
        Route::get('/report1', 'ReportController@tamuTahunan');
        Route::get('/report2', 'ReportController@pendapatanBulanan');
        Route::get('/report3', 'ReportController@pendapatanTahunan');
        Route::get('/report4', 'ReportController@jumlahTamu');
        Route::get('/report5', 'ReportController@top5Customer');
    });

});

